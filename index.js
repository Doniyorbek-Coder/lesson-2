// Your code here
// 1. Write a program that is divisible by 3 and 5
console.log("Task 1 >>>>>>>>>>>>")
let number = 100;
let array = [];
function threeFive(number){
    for(let i=0; i<number; i++){
        if(i%3 == 0 && i%5==0){
            array.push(i);
        }
    }
    console.log(array);
}

threeFive(number);

// 2. Write a program that checks whether given number odd or even
console.log("Task 2 >>>>>>>>>>>>")
let num = 50;

function oddEven(num){
    if(num%2==0){
        console.log(num + " is even")
    }else{
        console.log(num + " is odd")
    }
}

oddEven(num);

// 3. Write a program that get array as argument and sorts it by order // [1,2,3,4...]
console.log("Task 3 >>>>>>>>>>>>")
let arr = [32,23,45,7,1,89,278,0];

function sortArr(arr){
    arr.sort((a, b) => a - b);
}

sortArr(arr);
console.log("Sorting arrays: "+arr);
